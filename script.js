// Theory

// Для усунення зупинки всього коду через потрапляння пошкодженних або неправильних данних, для усунення непередбачуваних помилок.


class ArrayError extends Error {
  constructor(message, element){
    super(message)
    this.element = element;
  }
}


class List {
    constructor(array){
        this.array = array;
    }

    listCreation() {
        let list = document.createElement('ul');
        const root = document.querySelector("#root")
        root.append(list)
        this.array.forEach(element => { 
          try {
            if(element.author && element.name && element.price){
              let listItem = document.createElement('li');
              listItem.textContent = `${element.author}; ${element.name}; ${element.price}`;
              list.append(listItem);
            }
            else{
              throw new ArrayError('',  arr1.filter(el => !Object.keys(element).includes(el)) ) 
            }
          } catch (error) {
            console.log(error.message, `No such data: ${error.element}`);
          }
        });
    }

}

const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

let arr1 = Object.keys(books[0])

let a = new List(books);

a.listCreation()


